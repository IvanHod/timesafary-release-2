from rest_framework import serializers
from .models import Review
from timesafari.user.serializers import UserDisplayInfoSerializer

class ReviewSerializer(serializers.ModelSerializer):
    client = UserDisplayInfoSerializer()
    class Meta:
        model = Review
        fields = ('id', 'tour', 'client', 'rate', 'message', 'created', 'updated', )

class ReviewCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('id', 'tour', 'rate', 'message', 'created', 'updated', )
    def create(self, validated_data):
        client = validated_data.get('client')
        tour = validated_data.get('tour')

        if (client != tour.client):
            raise serializers.ValidationError({"detail": "Client did not own this tour"})
        
        review = Review.objects.create(**validated_data)
        return review