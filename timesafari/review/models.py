from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from timesafari.tour.models import Tour
from timesafari.user.models import TimeSafariUser

class Review(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE)
    client = models.ForeignKey(TimeSafariUser)
    rate = models.FloatField(default=1, validators=[MinValueValidator(1), MaxValueValidator(5)])
    message = models.TextField(blank=True)
    
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.tour.name

