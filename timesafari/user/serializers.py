from rest_framework import exceptions, serializers
from .models import TimeSafariUser

class UserDisplayInfoSerializer(serializers.ModelSerializer):
	class Meta:
		model = TimeSafariUser
		fields = (
			'id', 'email', 'first_name', 'last_name', 'patronimic', 'groups', 'physical_training', 'level_kind', 'picture',
		)

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = TimeSafariUser
		fields = (
			'id', 'email', 'first_name', 'last_name', 'patronimic', 'birthday', 'address',
			'phone', 'picture', 'groups', 'physical_training', 'level_kind',
		)

class UserUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = TimeSafariUser
		fields = (
			'id', 'email', 'first_name', 'last_name', 'patronimic', 'birthday', 'address',
			'phone', 'picture', 'physical_training', 'level_kind',
		)

class ConsultantUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = TimeSafariUser
		fields = (
			'id', 'physical_training', 'level_kind',
		)



class UserAdminSerializer(serializers.ModelSerializer):
	class Meta:
		model = TimeSafariUser
		fields = (
			'id', 'email', 'first_name', 'last_name', 'patronimic', 'birthday', 'address',
			'phone', 'picture', 'groups',
		)

		read_only_fields = (
			'email',
		)

class UserAdminChangeGroupSerializer(serializers.ModelSerializer):
	class Meta:
		model = TimeSafariUser
		fields = (
			'id', 'groups', 
		)
