from django import forms
from django.contrib import admin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import TimeSafariUser

class UserCreationForm(forms.ModelForm):

	password = forms.CharField(label = 'Password', widget = forms.PasswordInput)

	class Meta:
		model = TimeSafariUser
		fields = ('email', 'first_name', 'last_name', 'patronimic', 'password', 
					'birthday', 'address', 'phone', 'picture', 'physical_training', 'level_kind')

	def clean_password(self):
		password = self.cleaned_data.get("password")
		return password

	def save(self, commit = True):
		user = super(UserCreationForm, self).save(commit = False)
		user.set_password(self.cleaned_data["password"])
		if commit:
			user.save()
		return user

class UserChangeForm(forms.ModelForm):
	password = ReadOnlyPasswordHashField()

	class Meta:
		model = TimeSafariUser
		fields = ('email', 'first_name', 'last_name', 'patronimic', 'password', 'is_active', 'is_admin',
					'birthday', 'address', 'phone', 'picture', 'physical_training', 'level_kind')

	def clean_password(self):
		return self.initial['password']

class RegistrationForm(forms.ModelForm):
	password = forms.CharField(widget = forms.PasswordInput)

	class Meta:
		model = TimeSafariUser
		fields = ['email', 'first_name', 'last_name', 'patronimic', 'password',
					'birthday', 'address', 'phone', 'picture', ]

class LoginForm(forms.ModelForm):
	password = forms.CharField(widget = forms.PasswordInput)

	class Meta:
		model = TimeSafariUser
		fields = ['email', 'password']


