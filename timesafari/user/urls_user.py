from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from djoser import views as djoser_views


urlpatterns = [
	# /user/
	url(r'^$', views.UserListView.as_view(), name='user-list'),
	# /user/get/<pk>/
	url(r'^get/(?P<pk>[0-9]+)/$', views.UserDetailView.as_view(), name='user-detail'),
	# /user/<pk>/
	url(r'^(?P<pk>[0-9]+)/$', views.UserDetailView.as_view(), name='user-detail'),
	# /user/edit/<pk>/
	url(r'^edit/(?P<pk>[0-9]+)/$', views.UserChangeGroupView.as_view(), name='user-edit'),
	# /user/delete/<pk>/
	url(r'^delete/(?P<pk>[0-9]+)/$', views.UserDeleteView.as_view(), name='user-delete'),
	# /user/update/<pk>/
	url(r'^update/(?P<pk>[0-9]+)/$', views.ConsultantUpdateView.as_view(), name='user-update'),
]
