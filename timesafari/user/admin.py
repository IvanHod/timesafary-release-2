from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import TimeSafariUser
from .forms import UserChangeForm, UserCreationForm


class UserAdmin(BaseUserAdmin):

	form = UserChangeForm
	add_form = UserCreationForm

	list_display = ('email', 'first_name', 'last_name', 'patronimic', 'is_admin',)
	list_filter = ('is_admin',)

	fieldsets = (
		(None, {'fields': ('email', 'password')}),
		('Personal info', {'fields': ('first_name', 'last_name', 'patronimic', 'birthday', 'address', 'phone', 'picture', 'physical_training', 'level_kind' )}),
		('Permissions', {'fields': ('is_admin', 'groups')}),
	)

	add_fieldsets = (
		(None, {
			'classes': ('wide', ),
			'fields': ('email', 'first_name', 'last_name', 'patronimic', 'password', 'birthday', 'address', 'phone', 'picture', 'physical_training', 'level_kind'),
			}
		),
	)

	search_fields = ('email',)
	ordering = ('email',)
	filter_horizontal = ()

admin.site.register(TimeSafariUser, UserAdmin)
#admin.site.unregister(Group)

