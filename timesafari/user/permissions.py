from rest_framework import permissions
from .models import TimeSafariUser

class ConsultantUpdatePermission(permissions.BasePermission):
	def has_permission(self, request, view):
		if not request.user.is_authenticated:
			return False
		return request.user.groups.filter(name='Consultant').count() > 0
 
