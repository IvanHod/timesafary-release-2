from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin, Group
from django.core.validators import MinValueValidator, MaxValueValidator

class TimeSafariUserManager(BaseUserManager):
	def create_user(self, email, password, first_name=None, last_name=None, 
					patronimic=None, birthday=None, address=None, phone=None, picture=None):
		"""
		Creates and saves a Userss
		"""
		if not email:
			raise ValueError('Users must have an email address')
		if not password:
			raise ValueError('Password is empty')

		user = self.model(
			email = self.normalize_email(email),
			first_name = first_name,
			last_name = last_name,
			birthday = birthday,
			patronimic = patronimic,
			address = address,
			phone = phone,
			picture = picture,
			physical_training = 0,
			level_kind = 0,
			is_active = True,
		)

		user.set_password(password)
		user.save(using = self._db)
		user.groups.add(Group.objects.get(name='Client'))
		return user

	def create_superuser(self, email, password, first_name=None, last_name=None, 
						patronimic=None, birthday=None, address=None, phone=None, picture=None):
		"""
		Creates and saves a SuperUser
		"""
		user = self.create_user(
			email,
			password,
			first_name,
			last_name,
			patronimic,
			birthday,
			address,
			phone,
			picture
		)
		user.is_admin = True
		#user.is_superuser = True
		user.save(using = self._db)
		user.groups.add(Group.objects.get(name='Admin'))
		return user


class TimeSafariUser(AbstractBaseUser, PermissionsMixin):
	email = models.EmailField(
		verbose_name = 'email address',
		max_length = 255,
		unique = True,
	)

	first_name = models.CharField(max_length = 100, blank=True, null=True)
	last_name = models.CharField(max_length = 100, blank=True, null=True)
	patronimic = models.CharField(max_length = 100, blank=True, null=True)
	birthday = models.DateField(blank=True, null=True)
	address = models.CharField(max_length = 100, blank=True, null=True)
	phone = models.CharField(max_length = 20, blank=True, null=True)
	picture = models.FileField(blank=True, null=True)
	physical_training = models.PositiveIntegerField(default=0,
										validators=[MinValueValidator(0), MaxValueValidator(3)])

	level_kind = models.PositiveIntegerField(default=0,
										validators=[MinValueValidator(0), MaxValueValidator(3)])

	is_active = models.BooleanField(default = True)
	is_admin = models.BooleanField(default = False)

	objects = TimeSafariUserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['password']

	def get_full_name(self):
		return self.first_name + " " + self.last_name

	def get_short_name(self):
		return self.last_name

	def __str__(self):
		return self.email

	def has_perm(self, perm, obj = None):
		return True

	def has_module_perms(self, app_label):
		return True

	@property
	def is_staff(self):
		return self.is_admin
