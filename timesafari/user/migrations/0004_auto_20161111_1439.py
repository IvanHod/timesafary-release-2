# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-11 11:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_timesafariuser_patronimic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timesafariuser',
            name='patronimic',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
