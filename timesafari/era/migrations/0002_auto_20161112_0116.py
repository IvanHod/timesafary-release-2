# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-11 22:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('era', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='era',
            name='price',
            field=models.PositiveIntegerField(),
        ),
    ]
