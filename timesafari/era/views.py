from rest_framework import status, generics
from .models import Era
from .serializers import EraSerializer
from rest_framework import permissions
from .permissions import EraPermission
from timesafari.exceptions import DeletionError
from django.db.models.deletion import ProtectedError

class EraCreateView(generics.CreateAPIView):
	serializer_class = EraSerializer	
	permission_classes = (EraPermission, )

class EraListView(generics.ListAPIView):
	serializer_class = EraSerializer
	queryset = Era.objects.all()
	permission_classes = (permissions.AllowAny, )

class EraRetrieveView(generics.RetrieveAPIView):
	serializer_class = EraSerializer
	queryset = Era.objects.all()
	permission_classes = (permissions.AllowAny, )

class EraUpdateView(generics.UpdateAPIView):
	serializer_class = EraSerializer
	queryset = Era.objects.all()
	permission_classes = (EraPermission, )

class EraDeleteView(generics.DestroyAPIView):
	serializer_class = EraSerializer
	queryset = Era.objects.all()
	permission_classes = (EraPermission, )

	def perform_destroy(self, instance):
		try:
			instance.delete()
		except ProtectedError:
			raise DeletionError
