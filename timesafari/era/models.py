from django.db import models
from django.core.validators import MinValueValidator

class Era(models.Model):
	name = models.CharField(max_length=100)
	price = models.PositiveIntegerField(default=0,
										validators=[MinValueValidator(0)])
	picture = models.ImageField(blank=True)
	description = models.TextField(blank=True)

	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

