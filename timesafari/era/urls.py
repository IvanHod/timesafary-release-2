from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
	# /era/
	url(r'^$', views.EraListView.as_view(), name='era-list'),
	# /era/get/
	url(r'^get/$', views.EraListView.as_view(), name='era-list'),
	# /era/<pk>/
	url(r'^(?P<pk>[0-9]+)/$', views.EraRetrieveView.as_view(), name='era-detail'),
	# /era/get/<pk>/
	url(r'^get/(?P<pk>[0-9]+)/$', views.EraRetrieveView.as_view(), name='era-detail'),
	# /era/add/
	url(r'^add/$', views.EraCreateView.as_view(), name='era-create'),
	# /era/edit/<pk>/
	url(r'^edit/(?P<pk>[0-9]+)/$', views.EraUpdateView.as_view(), name='era-edit'),
	# /era/delete/<pk>/
	url(r'^delete/(?P<pk>[0-9]+)/$', views.EraDeleteView.as_view(), name='era-delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)