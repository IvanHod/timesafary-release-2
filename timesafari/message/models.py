from django.db import models
from timesafari.user.models import TimeSafariUser

class Message(models.Model):
	sender = models.ForeignKey(TimeSafariUser, related_name='sender')
	receiver = models.ForeignKey(TimeSafariUser, related_name='receiver')
	content = models.TextField()

	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	
	def __str__(self):
		return self.sender.email + ' to ' + self.receiver.email
