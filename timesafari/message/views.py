from rest_framework import status, generics
from django.db.models import Q
from .serializers import MessageCreateSerializer, MessageSerializer
from .models import Message
from .permissions import (MessageCreatePermission, 
							MessageUpdatePermission, 
							MessageDeletePermission,
							MessageViewPermission,)

class MessageCreateView(generics.CreateAPIView):
	serializer_class = MessageCreateSerializer
	permission_classes = (MessageViewPermission, )

	def perform_create(self, serializer):
		serializer.save(sender=self.request.user)

class MessageListView(generics.ListAPIView):
	serializer_class = MessageSerializer
	permission_classes = (MessageViewPermission, )

	def get_queryset(self):
		user = self.request.user
		queryset = Message.objects.filter(Q(sender=user) | Q(receiver=user)).order_by('created')
		return queryset

class MessageDeleteView(generics.DestroyAPIView):
	serializer_class = MessageSerializer
	queryset = Message.objects.all()
	permission_classes = (MessageDeletePermission, )

	def perform_destroy(self, instance):
		try:
			instance.delete()
		except ProtectedError:
			raise DeletionError

class MessageRetrieveView(generics.RetrieveAPIView):
	serializer_class = MessageSerializer
	permission_classes = (MessageViewPermission, )

	def get_queryset(self):
		user = self.request.user
		queryset = Message.objects.filter(Q(sender=user) | Q(receiver=user))
		return queryset
