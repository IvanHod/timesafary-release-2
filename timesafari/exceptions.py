from rest_framework.exceptions import APIException

class DeletionError(APIException):
    status_code = 400
    default_detail = 'Cannot delete this object because it depends on some other objects'
