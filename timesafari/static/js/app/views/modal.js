define([
    'backbone-validate',
    'bootstrap'
], function () {
    return Backbone.View.extend({

        title: 'Modal window',

        el: '#modal-window',

        currentPage: '',

        initialize: function (name) {
            if(name) {
                this.listenTo(App.Listener, name + 'ModelIsValid', _.bind(this.valid, this));
                this.listenTo(App.Listener, name + 'ModelIsUnValid', _.bind(this.unvalid, this));
            }
        },

        valid: function (fields) {
            var parent = this;
            if(fields.length > 0) {
                fields.forEach(function (field) {
                    var $input = parent.$el.find('[name="' + field + '"]');
                    if(field == 'event_date') {
                        $input
                            .closest('.date').parent()
                            .removeClass('has-error has-success')
                            .addClass('has-success')
                            .children('small').remove();
                    } else {
                        $input
                            .parent()
                            .removeClass('has-error has-success')
                            .addClass('has-success')
                            .children('small').remove();
                    }
                })
            }
        },

        unvalid: function (fields) {
            var parent = this;
            _.each(fields, function (msg, key) {
                var $input = parent.$el.find('[name="' + key + '"]');
                if(key == 'event_date') {
                    $input.closest('.date').parent().removeClass('has-error').children('small').remove();
                    $input.closest('.date').parent()
                        .addClass('has-error')
                        .append($('<small/>').html(msg).addClass('text-danger'));
                } else {
                    $input.parent().removeClass('has-error').children('small').remove();
                    $input.parent()
                        .addClass('has-error')
                        .append($('<small/>').html(msg).addClass('text-danger'));
                }
            })
        },

        render: function () {
            if(this.template) {
                this.$el.find('.modal-body').html(this.template(this.model.toJSON()));
                this.$el.modal();
            }
        },
        
        checkCurrentPage: function (page) {
            var currentPage = Backbone.history.location.hash.replace('#', '');
            currentPage = currentPage.replace(/\/\?[0-9a-z\=]+$|\/[0-9]+$/, '');
            return true;//page == currentPage;
        },

        clear: function () {
            this.$el.find('input').val('');
        },

        cancel: function () {}

    })
});