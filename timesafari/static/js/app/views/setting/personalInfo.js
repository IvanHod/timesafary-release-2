define([
    '../modal'
], function (Modal) {
    return Modal.extend({

        template: _.template($('#settings\\.personalInfo\\.tpl').html()),

        events: {
            'click .changePI'                   : 'changePI',
            'click #setting-pi .cancel-icon'    : 'cancel',
            'click #setting-pi .setting-pi span': 'changeCharacteristics'
        },

        render: function () {
            if(App.Profile)
                this.$el.find('.modal-body').html(this.template(App.Profile.toJSON()));
        },

        show: function () {
            this.render();
            this.$el.modal('show');
        },

        changePI: function (e) {
            var formData = {};
            var parent = this;
            if(!this.validate())
                return this;
            if(this.$el.find(':file').val())
                formData = new FormData(this.$el.find('form')[0]);
            else {
                formData = new FormData(this.$el.find('form')[0]);
                this.$el.find(':text').each(function (i, item) {
                    if($(item).val())
                            formData.append($(item).attr('name'), $(item).val());
                })
            }
            App.Profile.change(formData, {
                done: function () {
                    parent.cancel();
                },
                fail: function (err) {
                    parent.$el.find('.errors').html('');
                    _.each(function (item) {
                        parent.$el.find('.errors').append(item[0]);
                    });
                    console.error(err);
                }
            });
        },

        validate: function () {
            var $first_name = this.$el.find('[name="first_name"]'),
                $last_name = this.$el.find('[name="last_name"]');
            $first_name.parent().removeClass('has-error has-success');
            $last_name.parent().removeClass('has-error has-success');
            $first_name.next().remove();
            $last_name.next().remove();
            if(!$first_name.val()) {
                $first_name.parent().addClass('has-error');
                $first_name.after($('<small/>').addClass('text-danger').text('Это поле обязательное'));
                return false;
            } else {
                $first_name.parent().addClass('has-success');
            }
            if(!$last_name.val()) {
                $last_name.parent().addClass('has-error');
                $last_name.after($('<small/>').addClass('text-danger').text('Это поле обязательное'));
                return false;
            } else {
                $last_name.parent().addClass('has-success');
            }
            return true;
        },

        changeCharacteristics: function (e) {
            $(e.target).parent().find('span').removeClass('active');
            $(e.target).addClass('active');
            $(e.target).parent().prev().val($(e.target).index());
        },

        cancel: function (e) {
            this.$el.modal('hide');
        }

    })
});
