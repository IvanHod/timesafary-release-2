define([
    '../../models/user',
    '../../models/message',
    '../../collections/messages'
], function (User, Model, Messages) {

    return Backbone.View.extend({

        el: '#chat',

        template: _.template($('#chat\\.tpl').html()),

        template_message: {
            from: _.template($('#message\\.from\\.tpl').html()),
            to: _.template($('#message\\.to\\.tpl').html())
        },

        events: {
            'click .static-block'   : 'togglePanel',
            'keyup input.send'      : 'sendMessage',
            'click .chat-close'     : 'closeChat',
            'click .chat-turn'      : 'turnChat',
            'click .show-more'      : 'showMore'
        },

        messages: [],
        users: [],

        initialize: function (Peoples, chat) {
            this.chats = [];
            $('#chat').on('click', '.chat-turn', _.bind(this.turnChat, this))
                .on('click', '.chat-close', _.bind(this.closeChat, this))
                .on('click', '.show-more', _.bind(this.showMore, this))
                .on('keyup', 'input.send', _.bind(this.sendMessage, this))
                .on('click', 'input.send+div button:first', _.bind(this.sendMessageClick, this));

            this.listenTo(Peoples, 'openChat', _.bind(this.openChat, this));
            this.listenTo(App.Listener, 'newMessage', _.bind(function (data) {
                if(this.messages[data['sender_id']]) {
                    this.messages[data['sender_id']].add(new Model(data));
                    if(this.$el.find('#chat_' + data['sender_id']).hasClass('hidden'))
                        this.trigger('newMessage', data);
                } else {
                    this.trigger('newMessage', data);
                }
            }, this));
        },

        renderMessages: function (chat, messages) {
            var parent = this;
            chat.$el.find('.panel-body .show-more').remove();
            if(!messages.length)
                return;
            var $after = chat.$el.find('.panel-body')
                .prepend($('<div/>').addClass('show-more text-center').text('Показать еще'))
                .find('.show-more');
            messages.forEach(function (item) {
                item = item.toJSON();
                parent.appendMessage(chat['to'], item, $after);
            });
            if(messages.length < 10)
                $after.remove();
        },

        appendMessage: function (to, message, $after) {
            if(this.chats[to]) {
                message = _.extend(message, this.users[to].toJSON());
                if(!message['content'])
                    message['content'] = message['message'];
                if(!$after)
                    $after = this.chats[to].$el.find('.panel-body>div:last');
                var sender = message['sender_id'];
                var template_id = App['Profile'].get('id') == sender ? 'from' : 'to';
                $after.after(this.template_message[template_id](message));
                this.autoScroll(this.chats[to].$el.find('.panel-body'));
            }
        },

        hideChat: function (to) {
            var chats = this.$el.find('.panel.chat:not(.hidden)');
            var chat = this.$el.find('#chat_' + to + ':not(.hidden)');
            if(chats.length > 2 && !chat.length) {
                $(chats[chats.length - 1]).find('.chat-close').trigger('click');
            }
        },

        // to - id пользователя, с который необходимо открыть чат
        openChat: function (to) {
            if(!_.isNumber(to) && !_.isString(to))
                return;
            if(this.chats[to]) {
                this.hideChat(to);
                this.$el.find('#chat-' + to + ' .fio').removeClass('newMessages');
                var $chat = this.chats[to].$el;
                this.$el.prepend($chat);
                $chat.removeClass('hidden');
                if($chat.find('.panel-body').hasClass('hidden')) {
                    $chat.find('.chat-turn:visible').trigger('click');
                }
                this.autoScroll($chat.find('.panel-body'));
            } else {
                this.getMessages(to);
            }
            this.readMessages(to);
        },

        readMessages: function (to) {
            $.ajax({url: 'chat/seen/', type: 'POST', data: {sender_id: to}});
        },
        
        getMessages: function (to) {
            var chat = this.getChat(to);
            var offset = chat.$el.find('.message').length;
            this.messages[to].fetch({
                type: 'POST',
                data: {sender_id: App.Profile.get('id'), receiver_id: to, offset: offset},
                success: _.bind(function (messages) {
                    this.$el.find('#chat-' + to + ' .fio').removeClass('newMessages');
                    this.renderMessages(chat, messages);
                }, this)
            });
        },
        
        getChat: function (id) {
            var chat = this.chats[id];
            if(!chat) {
                var user = this.users[id] = new User();
                user.set({id: id}).fetch({
                    async: false,
                    success: _.bind(function (user) {
                        this.hideChat();
                        this.$el.prepend(this.template(user.toJSON()));
                        var messages = this.messages[id] = new Messages();
                        this.setListeners(messages);
                        chat = this.chats[id] = {
                            $el: this.$el.children().eq(0),
                            message: messages,
                            to: id
                        };
                    }, this),
                    error: function (err) {
                        console.error(err);
                    }
                });
            }
            return chat;
        },
        
        setListeners: function (collection) {
            var parent = this;
            this.listenTo(collection, 'add', function (model) {
                if(_.isString(model.get('sender_id')))
                    model.set('sender_id', parseInt(model.get('sender_id')));
                if(_.isString(model.get('receiver_id')))
                    model.set('receiver_id', parseInt(model.get('receiver_id')));
                var to = parent.chats[model.get('sender_id')] ? model.get('sender_id') : model.get('receiver_id');
                parent.appendMessage(to, model.toJSON());
            })
        },
        
        autoScroll: function ($body) {
            $body[0].scrollTop = 9999;
        },

        sendMessage: function (e) {
            var content = $(e.target).val();
            if(e.keyCode == 13 && content) {
                var parent = this;
                var to = $(e.target).data('to');
                var data = {
                    message: content,
                    sender_id: App.Profile.get('id'),
                    receiver_id: to
                };
                this.toggleSendBtn(to, true);
                parent.messages[to].create(data, {
                    success: function () {
                        $(e.target).val('');
                        parent.toggleSendBtn(to, false);
                    },
                    error: function (model, err) {
                        parent.toggleSendBtn(to, false);
                    }
                });
            }
        },

        sendMessageClick: function (e) {
            var button = $(e.currentTarget);
            if(!$(button).hasClass('hidden')) {
                this.sendMessage({target: $(button).parent().prev(), keyCode: 13});
            }
        },

        toggleSendBtn: function (to, spinner) {
            var $btn = this.chats[to].$el.find('.panel-footer button');
            if(spinner) {
                $btn.first().addClass('hidden');
                $btn.last().removeClass('hidden');
            } else {
                $btn.first().removeClass('hidden');
                $btn.last().addClass('hidden');
            }
        },

        closeChat: function (e) {
            $(e.target).closest('.panel').addClass('hidden');
            var $chat = $(e.target).closest('.chat');
            this.$el.append($chat);
        },

        turnChat: function (e) {
            $panel = $(e.target).closest('.panel');
            $panel.find('.panel-body').toggleClass('hidden');
            $panel.find('.panel-footer').toggleClass('hidden');
            $panel.find('.panel-heading span.chat-turn').toggleClass('hidden');
        },

        showMore: function (e) {
            var id = $(e.target).closest('.chat').attr('id').replace('chat_', '');
            this.getMessages(id);
        }
    })
});
