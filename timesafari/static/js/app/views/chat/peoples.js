define([
    './chat',
    '../../collections/users'
], function (Chats, Users) {
    return Backbone.View.extend({

        el: '#chat',

        template: _.template($('#chat\\.peoples\\.tpl').html()),

        template_people: _.template($('#chat\\.people\\.tpl').html()),

        events: {
            'click .static-block'   : 'togglePanel',
            'click .people'         : 'openChat',
            'keyup .search-peoples' : 'search'
        },

        beginFilter: '',

        defaultFilter: function (model) {
            var isGroup = !model.get('groups') || model.get('groups') && model.get('groups').indexOf(1) < 0;
            return model.get('id') != App.Profile.get('id') && isGroup;
        },

        initialize: function (chat) {
            this.filter = this.defaultFilter;
            this.users = new Users();
            if(chat && !_.isEmpty(chat)) {
                if(chat['beginFilter']) {
                    this.beginFilter = chat['beginFilter'];
                }
            }
            this.render();
        },

        render: function () {
            this.$el.html(this.template());
            this.$chatbody = this.$el.find('.chat-body');
            this.$count = this.$el.find('.count');
            this.users.fetch({data: this.beginFilter, reset: true, success: _.bind(this.renderUsers, this)});
            var chats = App.Views['Chats'] = new Chats(this, chat);
            this.listenTo(chats, 'newMessage', _.bind(this.newMessage, this));
            return this;
        },

        newMessage: function (data) {
            this.$el.find('#chat-' + data['sender_id']).find('.fio').addClass('newMessages');
        },

        renderUsers: function (users) {
            var parent = this;
            parent.$chatbody.html('');
            this.$count.text(users.filter(_.bind(this.defaultFilter, this)).length);
            users = this.filter ? users.filter(this.filter) : users;
            users.forEach(function (item) {
                item = item.toJSON();
                item.picture = item.picture ? item.picture : '/static/img/no_ava.gif';
                var fio = item.last_name + ' ' + item.first_name;
                if( fio.length > 16 )
                    fio = fio.substr(0, 13) + '...';
                item.fio = fio;
                parent.$chatbody.append(parent.template_people(item));
            });
            this.unreadChats();
        },

        unreadChats: function () {
            $.ajax({url:'chat/unseen'}).done(_.bind(function (chats) {
                if(chats && chats['id']) {
                    chats['id'].forEach(_.bind(function (id) {
                        this.$el.find('#chat-' + id + ' .fio').addClass('newMessages');
                    }, this));
                }
            }, this))
        },
        
        togglePanel: function (e) {
            this.$el.find('.chat-heading').toggleClass('hidden');
            $(e.currentTarget).find('.icon span').toggleClass('hidden');
        },

        openChat: function (e) {
            var id = $(e.currentTarget).data('id');
            this.trigger('openChat', id);
        },

        search: function (e) {
            var parent = this;
            var val = $(e.target).val();
            if(!val) {
                this.filter = this.defaultFilter;
                this.renderUsers(this.users);
                return;
            }
            this.filter = function (model) {
                var result = parent.defaultFilter(model);
                var fio = model.get('first_name') + ' ' + model.get('last_name');
                return result && fio.toUpperCase().indexOf(val.toUpperCase()) >= 0;
            };
            this.renderUsers(this.users);
        },
        
        destroy: function () {
            App.Views['Chats'].chats = [];
            App.Views['Chats'].stopListening();
            $('#chat').off();
            App.Views['Chats'].undelegateEvents();
            this.$el.children().remove();
            this.undelegateEvents();
        }
    })
});
