define([
    'backbone'
], function (Backbone) {
    return Backbone.View.extend({

        el: '#top-menu-picture',
        
        background: '#top-menu',

        template: _.template(
            '<div class="row profile-panel">' +
                '<div class="col-md-12 text-center">' +
                    '<% if(picture) { %>' +
                        '<div class="profile-ava"><img class="profile-image" src="<%-picture%>" /></div>' +
                    '<% } %>' +
                    '<div class="profile-name"><p>Добро пожаловать, <%- first_name %>!</p></div>' +
                '</div>' +
            '</div>'
        ),

        initialize: function () {
            this.listenTo(App.Profile, 'change', _.bind(this.render, this));
        },

        render: function () {
            //$(this.background).css({'background-image': 'url(/media/background.profile.jpg)'});
            this.$el.html('');
            if(App.Profile.get('id')) {
                var profile = App.Profile.toJSON();
                if(!profile['picture']) profile['picture'] = '/static/img/no_ava.gif';
                this.$el.append(this.template(profile));
            }
            return this;
        },

        removeContent: function () {
            $('.header').css({'background-image': ''});
            this.$el.html('');
        }
    })
});