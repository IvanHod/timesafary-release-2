define([
    '../collections/menu'
], function (Collection) {
    return Backbone.View.extend({

        el: '#top-menu',

        events: {
            'click .editSettings' : 'editSettings'
        },

        template: _.template('<div class="container"><ul class="nav navbar-nav navbar-right"></ul></nav>'),

        template_el: _.template('<li><a href="#<%=href%>"><%=name%></a></li>'),

        template_profile: _.template(
            '<li class="dropdown menu-profile">' +
                '<a href="#<%=href%>" class="col-md-7 col-xs-7 text-center"><%= first_name %></a>' +
                '<a class="dropdown-toggle col-md-5 col-xs-5" id="menu-profile" data-toggle="dropdown">' +
                    '<img src="<%= picture ? picture : "/static/img/no_ava.gif" %>">' +
                    '<b class="caret"></b>' +
                '</a>' +
                '<ul class="dropdown-menu">' +
                    '<li class="editSettings" data-el="profile"><a>Данные учетной записи</a></li>' +
                    '<li class="editSettings" data-el="personalInfo"><a>Персональные данные</a></li>' +
                    '<li class="divider"></li>' +
                    '<li><a href="#logout">Выход</a></li>' +
                '</ul>' +
            '</li>'
        ),

        collection : null,

        items: {
            client: 'home|profile|templates',
            consultant: 'home|profile|tours|templates',
            scientist: 'home|profile|era|kinds'
        },

        initialize: function (elements) {
            this.collection = new Collection(elements);
            this.render();
        },

        render: function () {
            var parent = this;
            this.$el.find('.container').remove();
            this.$el.prepend(this.template());
            this.collection.forEach(function (item) {
                if(item.get('active')) {
                    if(item.get('id') == 'profile')
                        parent.$el.find('ul').append(parent.template_profile(_.extend(item.toJSON(), App.Profile.toJSON())));
                    else
                        parent.$el.find('ul').append(parent.template_el(item.toJSON()));
                }
            });
            return this;
        },
        
        login: function (name) {
            this.collection.get('auth').set({active:false});
            this.collection.get('profile').set({active:true});
            this.render();
        },
        
        logout: function () {
            this.collection.get('auth').set({active:true});
            this.collection.get('profile').set({active:false});
            this.render();
        },
        
        setActive: function (el) {
            this.generate(el);
            this.render();
        },
        
        generate: function (el) {
            var parent = this;
            var homeItems = 'home|auth|logout|advantages|market';
            if(homeItems.indexOf(el) >= 0) {
                var items = 'home|advantages|market|' + (App.Profile.isAuth() ? 'profile' : 'auth');
                this.collection.forEach(function (model) {
                    model.set({active: items.indexOf(model.get('id')) >= 0});
                })
            } else {
                var group = App.Profile.getGroup();
                this.collection.forEach(function (model) {
                    model.set({active: parent['items'][group].indexOf(model.get('id')) >= 0});
                })
            }
        },

        editSettings: function (e) {
            App.Listener.trigger('editSettings', e);
        }

    })
});