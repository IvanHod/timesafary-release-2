define([
    '../modal',
    '../../models/kind',
    '../../collections/era'
], function (Modal, Model, Era) {
    return Modal.extend({

        template: _.template($('#kind\\.create\\.modal\\.tpl').html()),

        events: {
            'click #kind-create .cancel-icon'   : 'cancel',
            'click #kind-create #cancel'        : 'cancel',
            'click .kind-create'                : 'create'
        },

        isRender: false,

        initialize: function () {
            Modal.prototype.initialize.call(this, 'Kind');
        },

        render: function (id) {
            var parent = this;
            if(!this.isRender) {
                this.model = new Model();
                this.isRender = true;
            }
            if(id && _.isString(id)) {
                this.model.set({id:id}).fetch({success: function () {
                    parent.render();
                }});
                return this;
            }
            parent.isRender = false;
            Modal.prototype.render.call(this);
            this.eraValue = this.model.get('era');
            this.era = new Era();
            this.era.fetch({success: _.bind(this.resetEra, this)});
        },

        resetEra: function (collection) {
            var parent = this;
            var $select = this.$el.find('[name="era"]');
            $select.html('');
            collection.forEach(function (model) {
                var checked = parent.eraValue
                $select.append(
                    $('<option/>')
                        .attr('value', model.get('id'))
                        .html(model.get('name'))
                );
                if(parent.eraValue && parent.eraValue['id'] == model.get('id')) {
                    $select.children(':last').attr('selected', 'selected');
                }
            });
            this.eraValue = false;
        },

        create: function (e) {
            var parent = this,
                fields = {};
            this.$el.find('form textarea,input,select').each(function (i, item) {
                if($(item).is(':file') && $(item).val())
                    fields[$(item).attr('name')] = item.files[0];
                else
                    fields[$(item).attr('name')] = $(item).val();
            });
            this.model.set(fields).save(null, {success: function (model) {
                parent.trigger('EraCreated', model);
                parent.cancel();
            }});
        },

        showErrors: function (err) {
            if(_.isEmpty(err))  return;
            if(_.isObject(err)) err = _.values(err)[0];
            if(_.isArray(err))  err = err[0];
            var errorMsg = '<div class="alert alert-danger error-message text-center" role="alert">' + err + '</div>';
            this.$el.find('.error-message').remove();
            this.$el.find('.row:first').before(errorMsg);
        },

        cancel: function (e) {
            this.$el.modal('hide');
            if(this.isTable)
                App.Router.public.prevRouter();
            else
                App.Router.public.setRoute('profile');
        }

    })
});
