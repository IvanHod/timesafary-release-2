define([
    '../modal',
    '../../models/kind',
    '../../models/era'
], function (Modal, Model, Era) {
    return Modal.extend({

        template: _.template($('#kind\\.view\\.modal\\.tpl').html()),

        events: {
            'click #kind-view .cancel-icon' : 'cancel',
            'click .kind-delete'            : 'delete',
            'click #kindApprove'            : 'approve'
        },

        pressChange: false,

        render: function (id) {
            var model = this.model = new Model();
            model.set({id: id}).fetch({success: _.bind(this.renderData, this)});
        },
        
        renderData: function (model) {
            var merge = {
                danger: model.getDanger()
            };
            this.$el.find('.modal-body').html(this.template(_.extend(model.toJSON(), merge)));
            this.$el.modal();
            this.$el.find('span.era').html(model.get('name'));
            this.era = new Era();
            this.era.set({id: model.get('era')['id']}).fetch({success: _.bind(this.renderEra, this)});
        },
        
        renderEra: function (model) {
            this.$el.find('span.era').html(model.get('name'));
        },

        delete: function (e) {
            $.ajax({
                url: '/kind/delete/' + this.model.get('id') + '/',
                type: 'DELETE',
                data: {
                    id: this.model.get('id')
                },
                success: _.bind(this.cancel, this),
                error: _.bind(this.showErrors, this)
            });
        },

        showErrors: function (err) {
            if(_.isEmpty(err))  return;
            if(_.isObject(err)) err = err['responseJSON'];
            if(_.isObject(err))  err = err['detail'];
            if(_.isArray(err))  err = err[0];
            var errorMsg = '<div class="alert alert-danger error-message text-center" role="alert">' + err + '</div>';
            this.$el.find('.error-message').remove();
            this.$el.find('.row:first').before(errorMsg);
        },

        cancel: function (e) {
            this.$el.modal('hide');
            if(this.isTable) {
                this.isTable = false;
                App.Router.public.prevRouter();
            }
            else
                App.Router.public.setRoute('profile');
        }

    })
});
