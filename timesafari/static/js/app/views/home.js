define([
    'backbone',
    'bootstrap',
    './reviews/slider'
], function (Backbone, btstr, Reviews) {
    return Backbone.View.extend({

        el: '#general-page',

        template: _.template($('#home\\.tpl').html()),

        initialize: function () {
            this.reviews = new Reviews();
            this.listenTo(this.reviews, 'renderData', _.bind(this.renderReviews, this));
        },

        render: function () {
            this.$el.html(this.template());
            this.reviews.render();
        },

        renderReviews: function () {
            this.$el.find('#review-slider').html(this.reviews.el);
        }

    })
});