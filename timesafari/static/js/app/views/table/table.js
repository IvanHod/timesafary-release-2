define([
    //'./filter',
    'datatables',
    '../../collections/users',
    '../../collections/tours',
    '../../collections/era',
    '../../collections/kinds',
    '../../collections/templates'
], function (dt, Users, Tours, Era, Kinds, Templates) {
    return Backbone.View.extend({

        el: '#general-page',

        template: {
            'tour.consultant'       : _.template($('#tour\\.table\\.consultant\\.tpl').html()),
            'tour.client'           : _.template($('#tour\\.table\\.client\\.tpl').html()),
            'template.consultant'   : _.template($('#template\\.table\\.consultant\\.tpl').html()),
            'template.client'       : _.template($('#template\\.table\\.client\\.tpl').html()),
            'era.scientist'         : _.template($('#era\\.table\\.tpl').html()),
            'kind.scientist'        : _.template($('#kind\\.table\\.tpl').html())
        },

        template_tr: {
            'tour.consultant'       : _.template($('#tour\\.table\\.consultant\\.tr\\.tpl').html()),
            'tour.client'           : _.template($('#tour\\.table\\.client\\.tr\\.tpl').html()),
            'template.consultant'   : _.template($('#template\\.table\\.consultant\\.tr\\.tpl').html()),
            'template.client'       : _.template($('#template\\.table\\.client\\.tr\\.tpl').html()),
            'era.scientist'         : _.template($('#era\\.table\\.tr\\.tpl').html()),
            'kind.scientist'        : _.template($('#kind\\.table\\.tr\\.tpl').html())
        },

        initialize: function (collectionName) {
            this.name = collectionName + '.' + App.Profile.getGroup();
            this.elements = {clients : new Users()};
            if(collectionName == 'tour')    this.collection = new Tours();
            else                            this.elements['tours'] = new Tours();
            if(collectionName == 'era')     this.collection = new Era();
            else                            this.elements['era'] = new Era();
            if(collectionName == 'kind')    this.collection = new Kinds();
            else                            this.elements['kinds'] = new Kinds();
            if(collectionName == 'template')this.collection = new Templates();
            else                            this.elements['templates'] = new Templates();
        },
        
        render: function (filter) {
            //this.filter.render(filter);
            this.$el.html(this.template[this.name]());
            this.collection.fetch({async:false, success: _.bind(this.renderData, this)});
            _.keys(this.elements).forEach(_.bind(function (key) {
                this.elements[key].fetch({
                    success: _.bind(function (collection) {
                        this.fullTable(key, collection);
                    }, this)
                });
            }, this));
        },
        
        renderData: function (collection) {
            var fields = [];
            var parent = this;
            this.$el.find('thead th').each(function (i, item) {
                fields.push($(item).data('name'));
            });
            var $tbody = this.$el.find('tbody');
            collection.forEach(function (model) {
                var tr = {};
                fields.forEach(function (field) {
                    var fields = field.split('.');
                    var value = model.get(fields[0]);
                    if(_.isObject(value)) {
                        value = value[fields[1]]
                    }
                    if(field == 'danger')   value = model.getDanger();
                    if(field == 'status')   {
                        value = model.getStatus();
                        tr['href'] = model.getStatusHrefConsultant();
                    }
                    tr[fields[0]] = value;
                });
                $tbody.append(parent.template_tr[parent.name](tr));
            });
            this.$el.find('table').DataTable({
                "language": {
                    "lengthMenu": "Показать _MENU_ записей на странице",
                    "zeroRecords": "Записей не найденно",
                    "info": "Показано с _START_ по _END_ из _TOTAL_ записей",
                    "infoEmpty": "Записей не найдено",
                    "loadingRecords": "Загрузка...",
                    "processing":     "В процессе...",
                    "search":         "Поиск:",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Первая",
                        "last":       "Последняя",
                        "next":       "Следующая",
                        "previous":   "Предыдущая"
                    }
                }
            })
        },
        
        fullTable: function (key, collection) {
            this.$el.find('[data-kind="' + key + '"]').each(_.bind(function (i, el) {
                var id = $(el).data('id');
                var model = collection.get(id);
                var name = model.get('name');
                if(!name && key == 'clients') {
                    name = model.get('last_name') + ' ' + model.get('first_name')
                }
                $(el).html(name);
            }, this));
        },
        
        createFilter: function () {
            
        }

    })
});