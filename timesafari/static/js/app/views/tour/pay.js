define([
    '../modal',
    '../../models/tour',
    '../../models/pay'
], function (Modal, Model, Pay) {
    return Modal.extend({

        template: _.template($('#tour\\.pay\\.modal\\.tpl').html()),

        currentPage: 'tour/pay',

        events: {
            'click #tour-pay #pay-back'     : 'cancel',
            'click #tour-pay .cancel-icon'  : 'cancel',
            'click #tour-pay #pay'          : 'pay'
        },

        initialize: function () {
            Modal.prototype.initialize.call(this, 'Pay');
        },

        render: function (id) {
            var parent = this;
            if(id && _.isString(id)) {
                this.model = new Model();
                this.model.set({id:id}).fetch({success: function () {
                    parent.render();
                }});
                return this;
            }
            Modal.prototype.render.call(this);
        },

        pay: function (e) {
            var fields = {},
                parent = this;
            this.$el.find('form input').each(function (i, item) {
                fields[$(item).attr('name')] = $(item).val();
            });
            var pay = new Pay();
            if( pay.set(fields).validate() ) {
                this.model.set({ paid: true}).save(null, {
                    success: function () {
                        parent.cancel();
                    }
                });
            }
        },

        cancel: function (e) {
            this.$el.modal('hide');
            setTimeout(_.bind(function () {
                App.Router.public.setRoute('tour/client/' + this.model.get('id'));
            }, this), 500);
        }

    })
});
