define([
    '../models/tour',
    '../models/kind',
    '../models/era'
], function (Tour, Kind, Era) {
    return {
        'tour' : Backbone.Collection.extend({

            url : '/tour/',

            model: Tour,

            addParam: function (param) {
                if(this.url.indexOf('?') > 0)
                    this.url += '&';
                else
                    this.url += '?';
                this.url += param['name'] + '=' + param['value'];
            },
            
            initialize: function (params) {
                if(_.isString(params)) {
                    if(params[0] != '?') params = '?' + params;
                } else if(_.isArray(params)) {
                    params = '?' + params.join('&');
                }
                this.url += params ? params : '';
            }

        }),
        'kind' : Backbone.Collection.extend({

            url : '/kind/',

            model: Kind

        }),
        'era' : Backbone.Collection.extend({

            url : '/era/',

            model: Era

        }),
        'message' : Backbone.Collection.extend({

            url : '/message/',

            model: Era

        })
    }
});