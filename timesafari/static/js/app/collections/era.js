define([
    '../models/era'
], function (Model) {
    return Backbone.Collection.extend({

        url : 'era/',

        model: Model

    })
});