define([
    '../models/review'
], function (Model) {
    return Backbone.Collection.extend({

        url : '/review/',

        model: Model

    })
});