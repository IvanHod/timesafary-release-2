define([
    '../models/menu'
], function (Model) {
    return Backbone.Collection.extend({

        url : '/menu',

        model: Model

    })
});