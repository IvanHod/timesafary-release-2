define([
    'backbone'
], function () {
    return Backbone.Model.extend({

        idAttribute: 'id',

        urlRoot: 'user/'

    })
});