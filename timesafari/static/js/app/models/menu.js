define([
    'backbone'
], function (Backbone) {
    return Backbone.Model.extend({

        idAttribute: 'id',

        defaults: {
            name: '',
            active: false
        }

    })
});