requirejs.config({
    baseUrl: 'static/js',
    paths: {
        'jquery'                : 'libs/jquery-3.1.1.min',
        'jquery.cookie'         : 'libs/jquery.cookie',
        'underscore'            : 'libs/underscore',
        'backbone'              : 'libs/backbone',
        'backbone-validate'     : 'libs/backbone-validation-min',
        'backbone-file-upload'  : 'libs/backbone-model-file-upload',
        'bootstrap'             : 'libs/bootstrap.min',
        'bootstrap-validator'   : 'libs/bootstrap-validator.min',
        'pusher'                : 'libs/pusher.min',
        'datepicker'            : 'libs/bootstrap-datepicker.min',
        'datatables'            : 'libs/jquery.dataTables.min'
    },
    shim: {
        'jquery.cookie'         : ['jquery'],
        'underscore'            : ['jquery'],
        'backbone'              : ['underscore', 'jquery'],
        'backbone-validate'     : ['backbone', 'underscore', 'jquery'],
        'backbone-file-upload'  : {
            deps: ['jquery', 'underscore', 'backbone'],
            exports: 'Backbone'
        },
        'bootstrap'             : ['jquery'],
        'bootstrap-validator'   : ['bootstrap', 'jquery'],
        'datepicker'            : ['bootstrap', 'jquery'],
        "datatables"            : ['jquery'],
        "datatables-bootstrap"  : ['jquery', 'datatables', 'bootstrap']
    }
});

// Start the main app logic.
requirejs([
    'app',
    'app/models/profile',
    'app/views/profile',
    'app/views/tour/create',
    'app/views/tour/pay',
    'app/views/tour/view',
    'app/views/template/create',
    'app/views/template/view',
    'app/views/era/create',
    'app/views/era/view',
    'app/views/kind/create',
    'app/views/kind/view',
    'app/views/chat/peoples',
    'app/router',
    'app/views/menu',
    'app/views/auth/auth',
    'app/views/home',
    'app/views/table/table',
    'pusher'
], function(app, Profile, ViewProfile, TourCreate, TourPay, TourView, TemplateCreate, TemplateView,
            EraCreate, EraView, KindCreate, KindView, ChatPeoples, Router, Menu, Login, Home, Tables) {
    App.Listener = new Backbone.Model();
    App.Profile = new Profile();
    App.Profile.fetch({success: success, error: error});
    App.Views['home'] = new Home();
    var menu = App.Views["Menu"] = new Menu([
        {id: 'home', name: 'О нас', href: 'home', active: true},
        {id: 'advantages', name: 'Наши преимущества', href: 'advantages', active: true},
        {id: 'market', name: 'Конструктор туров', href: 'market', active: true},
        {id: 'auth', name: 'Войти/Зарегестрироваться', href: 'auth', active: true},
        {id: 'tours', name: 'История туров', href: 'tours', active: false},
        {id: 'templates', name: 'Магазин туров', href: 'templates', active: false},
        {id: 'era', name: 'Эры', href: 'periods', active: false},
        {id: 'kinds', name: 'Виды животных', href: 'kinds', active: false},
        {id: 'profile', name: 'Профиль', href: 'profile', active: false}
    ]);
    App.Views['login'] = new Login();
    App.Views['login'].listenTo(App.Profile, 'login', function () {
        App.Profile.fetch({success: success});
    });
    App.Views['login'].listenTo(App.Profile, 'registration', App.Profile.auth);
    App.Router.public = new Router(menu);
    Backbone.history.start({pushState: false});
    eventClickForChat();

    $(document).on('click', '#modal-window', function(e) {
        if($(e.target).hasClass('modal')) {
            e.preventDefault();
            e.stopImmediatePropagation();
            $('.cancel-icon').trigger('click');
            return false;
        }
    });

    function success() {
        var group = App.Profile.getGroup();
        var settings = app.settings[group];
        if(group == 'client') {
            settings.panels[0].collection.addParam({name: 'client', value: App.Profile.get('id')})
            settings.panels[1].collection.addParam({name: 'client', value: App.Profile.get('id')})
        }
        App.Views['Profile'] = new ViewProfile(settings);
        createViews(group, settings);
        App.Views['Profile'].listenTo(App.Profile, 'logout', function () {
            App.Views['Profile'].removeHeader();
            menu.logout();
            pusherDisconnect();
            removeViews();
        });
        pusherConnection();
        menu.login();
        App.Views['login'].cancel();
    }


    function error() {}
    
    function createViews(group, settings) {
        if('client|admin|consultant'.indexOf(group) >=0) {
            App.Views['Tour.Create'] = new TourCreate();
            App.Views['Tour.View'] = new TourView();
            App.Views['Tours'] = new Tables('tour');
            App.Views['Templates'] = new Tables('template');
            App.Views['Template.Create'] = new TemplateCreate();
            App.Views['Template.View'] = new TemplateView();
        }
        if(group == 'client') {
            App.Views['Tour.Pay'] = new TourPay();
        }
        if('scientist|admin'.indexOf(group) >= 0) {
            App.Views['Era.Create'] = new EraCreate();
            App.Views['Era.View'] = new EraView();
            App.Views['Kind.Create'] = new KindCreate();
            App.Views['Kind.View'] = new KindView();
            App.Views['Era'] = new Tables('era');
            App.Views['Kinds'] = new Tables('kind');
        }
        App.Views['ChatPeoples'] = new ChatPeoples(settings['chat']);
    }
    
    function removeViews() {
        if(App.Views['Profile'])        App.Views['Profile'].undelegateEvents();
        if(App.Views['Tour.Create'])    App.Views['Tour.Create'].undelegateEvents();
        if(App.Views['Tour.Pay'])       App.Views['Tour.Pay'].undelegateEvents();
        if(App.Views['Tour.View'])      App.Views['Tour.View'].undelegateEvents();
        if(App.Views['Era.Create'])     App.Views['Era.Create'].undelegateEvents();
        if(App.Views['Era.View'])       App.Views['Era.View'].undelegateEvents();
        if(App.Views['Kind.Create'])    App.Views['Kind.Create'].undelegateEvents();
        if(App.Views['Kind.View'])      App.Views['Kind.View'].undelegateEvents();
        if(App.Views['Template.Create'])App.Views['Template.Create'].undelegateEvents();
        if(App.Views['Template.View'])  App.Views['Template.View'].undelegateEvents();
        if(App.Views['ChatPeoples'])    App.Views['ChatPeoples'].destroy();
    }

});

function pusherConnection() {
    if(!App.Pusher) {
        App.Pusher = new Pusher('e9f4c44c23228e97eb04', {
            cluster: 'eu',
            authEndpoint: '/chat/auth/',
            auth: {
                headers: {
                    'X-CSRFToken': $.cookie('csrftoken')
                }
            }
        });
    }
    channel_name = 'private-notifications-' + App.Profile.get('id');
    notifications = App.Pusher.subscribe(channel_name);
    App.Pusher.connection.bind('connected', function () {
        notifications.bind('new_message', function (data) {
            App.Listener.trigger('newMessage', data);
        });
    });
}

function pusherDisconnect() {
    App.Pusher.disconnect();
}

function eventClickForChat() {
    $(document).on('click', function (e) {
        var $chatBackground = $('#chat-black-background');
        var chat = $(e.target).closest('#chat');
        var willHide = !!chat.length;
        if($(e.target).hasClass('chat-close')) {
            willHide = false;
        }
        if(willHide) {
            $chatBackground.css({display: 'block'});
        } else {
            $('.panel.chat:visible .chat-turn.glyphicon-chevron-down:visible').trigger('click');
            $chatBackground.css({display: 'none'});
        }
    })
}