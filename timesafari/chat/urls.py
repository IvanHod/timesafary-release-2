from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^$', views.chat_index, name='chat_index'),
    url(r'^auth/$', views.chat_auth, name='chat_auth'),
    url(r'^send/$', views.chat_send, name='chat_send'),
    url(r'^join/$', views.chat_join, name='chat_join'),
    url(r'^unseen/$', views.chat_unseen, name='chat_unseen'),
    url(r'^seen/$', views.chat_seen, name='chat_seen'),
]

urlpatterns = format_suffix_patterns(urlpatterns)