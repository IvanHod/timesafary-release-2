from rest_framework import serializers
from .models import Tour, Template
from timesafari.user.serializers import UserDisplayInfoSerializer
from timesafari.era.serializers import EraDisplaySerializer
from timesafari.era.models import Era
from timesafari.kind.models import Kind
from datetime import datetime, timedelta
from .utils import generate_doc
from django.db.models import F



class TourSerializer(serializers.ModelSerializer):
	client = UserDisplayInfoSerializer()
	assigned_personal = UserDisplayInfoSerializer()
	class Meta:
		model = Tour
		fields = ('id', 'name', 'danger', 'era', 'kind', 'event_date', 'price', 
			'duration', 'client', 'assigned_personal', 'paid', 'status',
			'created', 'updated', 'pay_date', 'document_link')

class TourUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tour
		exclude = ('client', 'assigned_personal', 'created', 'updated', 'name', )

	# Add pay_date when consultant changes status to PROCESSING
	def update(self, instance, validated_data):
		status = validated_data.get("status", None)
		if instance.status == 0 and status == 1:
			instance.pay_date = datetime.now().date() + timedelta(days = 2)
			instance.status = 1
			instance.save()

		old_kind = instance.kind
		new_kind = validated_data.get('kind', instance.kind)
		event_date = validated_data.get('event_date', instance.event_date)
		if new_kind != old_kind:
			if event_date == None or event_date < datetime.now().date():
				if new_kind.count == 0:
					raise serializers.ValidationError({"detail": "Данных животных нет на текущий момент"})
				else:
					old_kind.count += 1
					old_kind.save()
					new_kind.count -= 1
					new_kind.save()
		else:
			if (instance.status == 0 or instance.status == 1) and status == 3:
				old_kind.count += 1
				old_kind.save()

		instance.danger = validated_data.get('danger', instance.danger)
		instance.era = validated_data.get('era', instance.era)
		instance.kind = validated_data.get('kind', instance.kind)
		instance.event_date = validated_data.get('event_date', instance.event_date)
		instance.price = validated_data.get('price', instance.price)
		instance.duration = validated_data.get('duration', instance.duration)

		if instance.paid == False and validated_data.get("paid", None) == True:
			instance.document_link = generate_doc(instance)

		instance.paid = validated_data.get('paid', instance.paid)
		instance.status = validated_data.get('status', instance.status)

		instance.save()
		
		return instance

class TourCreateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tour
		exclude = ('client', 'assigned_personal', 'status', )
		read_only_fields = ('id', )

	def create(self, validated_data):
		client = validated_data.get('client')
		event_date = validated_data.get('event_date')

		if (client != None and event_date != None):
			found = (Tour.objects.filter(client = client, event_date = event_date).count())
			if found:
				raise serializers.ValidationError({"detail": "Клиент может заказывать один тур на одну дату"})

		kind = validated_data.get('kind')
		if F('count') == 0:
			raise serializers.ValidationError({"detail": "Данных животных нет на текущий момент"})
		else:
			kind.count = F('count') - 1
			kind.save()

		tour = Tour.objects.create(**validated_data)
		self.update(tour, validated_data)
		return tour

	def update(self, instance, validated_data):
		name = validated_data['era'].name + " #" + str(instance.id)
		instance.name = name
		instance.save()
		return instance

class TourExtendSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tour
		fields = ('pay_date', )

	def update(self, instance, validated_data):
		if instance.pay_date < datetime.now().date():
			instance.pay_date = datetime.now().date() + timedelta(days = 2)
			instance.status = 1
			instance.save()
		return instance



class TemplateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Template
		fields = ('id', 'name', 'danger', 'era', 'kind', 'price', 
			'duration', 'created', 'updated', )

class TemplateCreateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Template
		fields = ('name', 'danger', 'era', 'kind', 'price', 'duration', )		
		read_only_fields = ('id', )

	def create(self, validated_data):
		kind = validated_data.get('kind')
		if kind.count == 0:
			raise serializers.ValidationError({"detail": "Данных животных нет на текущий момент"})
		else:
			print("Template create")
			kind.count -= 1
			kind.save()

		template = Template.objects.create(**validated_data)
		self.update(template, validated_data)
		return template

	def update(self, instance, validated_data):
		name = "template " + validated_data['era'].name + " #" + str(instance.id)
		instance.name = name
		instance.save()
		return instance

class TemplateUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Template
		fields = ('id', 'name', 'danger', 'era', 'kind', 'price', 
			'duration', 'created', 'updated', )

	def update(self, instance, validated_data):
		old_kind = instance.kind
		new_kind = validated_data.get('kind', None)
		if new_kind != None and old_kind != new_kind:
			if new_kind.count == 0:
				raise serializers.ValidationError({"detail": "Данных животных нет на текущий момент"})
			else:
				old_kind.count += 1
				old_kind.save()
				new_kind.count -= 1
				new_kind.save()
		instance.danger = validated_data.get('danger', instance.danger)
		instance.era = validated_data.get('era', instance.era)
		instance.kind = validated_data.get('kind', instance.kind)
		instance.price = validated_data.get('price', instance.price)
		instance.duration = validated_data.get('duration', instance.duration)
		instance.save()
		return instance
