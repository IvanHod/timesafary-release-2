from django.db import models
from timesafari.user.models import TimeSafariUser
from timesafari.era.models import Era
from timesafari.kind.models import Kind
from django.core.validators import MinValueValidator, MaxValueValidator

class Template(models.Model):
	name = models.CharField(max_length=100, blank=True)
	DANGER_LEVELS = (
		(0, 'Low'),
		(1, 'Medium'),
		(2, 'High'),
		(3, 'Very High'),
	)
	danger = models.PositiveIntegerField(default=0, choices=DANGER_LEVELS,
										validators=[MinValueValidator(0), MaxValueValidator(3)])
	era = models.ForeignKey(Era)
	kind = models.ForeignKey(Kind)
	price = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])
	duration = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Tour(models.Model):
	name = models.CharField(max_length=100, blank=True)
	DANGER_LEVELS = (
		(0, 'Low'),
		(1, 'Medium'),
		(2, 'High'),
		(3, 'Very High'),
	)
	danger = models.PositiveIntegerField(default=0, choices=DANGER_LEVELS,
										validators=[MinValueValidator(0), MaxValueValidator(3)])

	era = models.ForeignKey(Era, on_delete=models.PROTECT)
	kind = models.ForeignKey(Kind, on_delete=models.PROTECT)
	event_date = models.DateField(null=True, blank=True)
	price = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])
	duration = models.PositiveIntegerField(default=0, validators=[MinValueValidator(0)])
	client = models.ForeignKey(TimeSafariUser, related_name='client')
	assigned_personal = models.ForeignKey(TimeSafariUser, related_name='consultant', null=True, blank=True)
	pay_date = models.DateField(null=True, blank=True)
	paid = models.BooleanField(default=False)

	STATUS_TYPES = (
		(0, 'Waiting'),
		(1, 'Processing'),
		(2, 'Finish'), 
		(3, 'Passed'),
	)
	status = models.IntegerField(choices=STATUS_TYPES, blank=True, default=0,
							validators=[MinValueValidator(0), MaxValueValidator(3)])

	document_link = models.FileField(blank=True, null=True)	
							
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

