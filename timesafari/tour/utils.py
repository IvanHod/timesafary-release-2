from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm, inch
from reportlab.platypus import Paragraph, Frame
from reportlab.lib.styles import getSampleStyleSheet

from reportlab.pdfbase import pdfmetrics 
from reportlab.pdfbase.ttfonts import TTFont
from django.conf import settings
import os
from datetime import datetime

def generate_doc(tour):
    if tour == None:
        return

    pdfmetrics.registerFont(TTFont('Times-New-Roman', 'times.ttf')) 
    pdfmetrics.registerFont(TTFont('Times-New-RomanBd', 'timesBd.ttf')) 

    styleSheet = getSampleStyleSheet()
    style = styleSheet['Normal']
    style.alignment = 0
    style.fontName = "Times-New-Roman"
    style.fontSize = 12
    style.spaceAfter = 0.2 * cm

    filename = str(tour.client.id) + "_" + str(tour.id) + "_" + datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + ".pdf"
    ret = settings.DOCUMENT_URL + filename
    filename = os.path.join(settings.DOCUMENT_ROOT, filename)
    c = canvas.Canvas(filename, pagesize = A4)
    width, height = A4
    margin_top = 2 * cm
    margin_right = 1.5 * cm
    margin_bottom = 2 * cm
    margin_left = 3 * cm
    line_unit = -0.5 * cm
    width -= margin_left + margin_right
    height -= margin_top + margin_bottom
    c.setFont("Times-New-RomanBd", 12)
    c.translate(margin_left, margin_top)
    c.drawCentredString(width / 2, height, u"Договор на предоставление туристических услуг".encode('utf-8'))
    c.translate(0, line_unit)
    c.drawCentredString(width / 2, height, ("№ " + str(tour.id) + " от " + datetime.now().strftime("%d.%m.%Y")).encode('utf-8'))
    c.translate(0, line_unit)
    c.drawString(0, height, u"г. Волгоград".encode('utf-8'))

    c.translate(0, line_unit * 3)
    c.drawCentredString(width / 2, height, u"Предмет договора".encode('utf-8'))

    c.translate(0, line_unit * 1.5)
    f = Frame(0, 0, width, height, leftPadding = 0, bottomPadding = 0,
            rightPadding = 0, topPadding = 0, showBoundary = 0)
    story = []
    story.append(Paragraph(u"Компания «TimeSafari» реализует тур для путешествия в прошлое с возможностью охоты на выбранное клиентом животное. Компания не обеспечивает страховку клиенту.".encode('utf-8'), style))
    f.addFromList(story, c)

    c.setFont("Times-New-Roman", 12)

    c.translate(0, line_unit * 4)

    client_name = ""
    if tour.client.last_name:
        client_name += tour.client.last_name + " "
    if tour.client.first_name:
        client_name += tour.client.first_name + " "
    if tour.client.patronimic:
        client_name += tour.client.patronimic
    assigned_personal_name = ""
    if tour.assigned_personal.last_name:
        assigned_personal_name += tour.assigned_personal.last_name + " "
    if tour.assigned_personal.first_name:
        assigned_personal_name += tour.assigned_personal.first_name + " "
    if tour.assigned_personal.patronimic:
        assigned_personal_name += tour.assigned_personal.patronimic
    
    if client_name:
        c.drawString(0, height, ("Клиент: " + client_name + ".").encode('utf-8'))
    else:
        c.drawString(0, height, ("Клиент: ____________________________.").encode('utf-8'))        

    c.translate(0, line_unit * 2)
    # TODO: What is Дата отправки
    c.drawString(0, height, ("Дата отправки: " + tour.event_date.strftime("%d.%m.%Y") + "г.").encode('utf-8'))

    c.translate(0, line_unit * 2)
    c.drawString(0, height, ("Эра: " + tour.era.name + ".").encode('utf-8'))

    c.translate(0, line_unit * 2)
    c.drawString(0, height, ("Вид животного: " + tour.kind.name + ".").encode('utf-8'))

    DANGER_LEVELS = ['Низкая', 'Средняя', 'Высокая', 'Очень высокая']

    c.translate(0, line_unit * 2)
    c.drawString(0, height, ("Опасность: " + DANGER_LEVELS[tour.danger] + ".").encode('utf-8'))

    c.translate(0, line_unit * 2)
    c.drawString(0, height, ("Оплаченная сумма: " + str(tour.price) + "$.").encode('utf-8'))

    c.setFont("Times-New-RomanBd", 12)
    c.translate(0, line_unit * 4)
    c.drawCentredString(width / 2, height, u"Адреса и реквизиты сторон".encode('utf-8'))

    # 1st column
    c.translate(0, line_unit * 2)
    f1 = Frame(0, 0, width / 2 - 0.5 * cm, height, leftPadding = 0, bottomPadding = 0,
            rightPadding = 0, topPadding = 0, showBoundary = 0)
    story = []
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Фирма:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    story.append(Paragraph(u"ООО «TimeSafari»".encode('utf-8'), style))
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Консультант по работе с клиентом:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    if assigned_personal_name:
        story.append(Paragraph(assigned_personal_name.encode('utf-8'), style))
    else:
        story.append(Paragraph(u"____________________________".encode('utf-8'), style))        
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Email:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    story.append(Paragraph(tour.assigned_personal.email.encode('utf-8'), style))
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Телефон:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    if tour.assigned_personal.phone:
        story.append(Paragraph(tour.assigned_personal.phone.encode('utf-8'), style))
    else:
        story.append(Paragraph(u"____________________________".encode('utf-8'), style))        
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Генеральный директор:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    story.append(Paragraph(u"__________ (Ходненко И. В.)".encode('utf-8'), style))
    f1.addFromList(story, c)

    # 2nd column
    f2 = Frame(width / 2 + 0.5 * cm, 0, width / 2 - 0.5 * cm, height, leftPadding = 0, bottomPadding = 0,
            rightPadding = 0, topPadding = 0, showBoundary = 0)
    story = []
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Заказчик:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    if client_name:
        story.append(Paragraph(client_name.encode('utf-8'), style))
    else:
        story.append(Paragraph(u"____________________________".encode('utf-8'), style))        
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Email:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    story.append(Paragraph(tour.client.email.encode('utf-8'), style))
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Телефон:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    if tour.client.phone:
        story.append(Paragraph(tour.client.phone.encode('utf-8'), style))
    else:
        story.append(Paragraph(u"____________________________".encode('utf-8'), style))        
    style.fontName = "Times-New-RomanBd"
    story.append(Paragraph(u"Адрес прописки:".encode('utf-8'), style))
    style.fontName = "Times-New-Roman"
    if tour.client.address:
        story.append(Paragraph(tour.client.address.encode('utf-8'), style))
    else:
        story.append(Paragraph(u"____________________________".encode('utf-8'), style))        
    f2.addFromList(story, c)

    story = []
    style.spaceBefore = 4 * style.spaceAfter
    story.append(Paragraph(u"__________ (_______________________)".encode('utf-8'), style))
    f2.addFromList(story, c)

    story = []
    style.spaceBefore = 0
    style.leftIndent = 4 * cm
    story.append(Paragraph(u"Ф.И.О.".encode('utf-8'), style))
    f2.addFromList(story, c)

    c.showPage()
    c.save()

    return ret