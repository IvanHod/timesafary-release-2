from django.contrib import admin
from .models import Tour, Template

admin.site.register(Tour)
admin.site.register(Template)

