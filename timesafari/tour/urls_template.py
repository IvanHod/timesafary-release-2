from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
	# /template/
	url(r'^$', views.TemplateListView.as_view(), name='template-list'),
	# /template/get
	url(r'^get/$', views.TemplateListView.as_view(), name='template-list'),
	# /template/<pk>/
	url(r'^(?P<pk>[0-9]+)/$', views.TemplateRetrieveView.as_view(), name='template-detail'),
	# /template/get/<pk>/
	url(r'^get/(?P<pk>[0-9]+)/$', views.TemplateRetrieveView.as_view(), name='template-detail'),
	# /template/add/
	url(r'^add/$', views.TemplateCreateView.as_view(), name='template-create'),
	# /template/edit/<pk>/
	url(r'^edit/(?P<pk>[0-9]+)/$', views.TemplateUpdateView.as_view(), name='template-edit'),
	# /template/delete/<pk>/
	url(r'^delete/(?P<pk>[0-9]+)/$', views.TemplateDeleteView.as_view(), name='template-delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)