from django.core.management import BaseCommand
from timesafari.tour.models import Tour
from timesafari.message.models import Message
import datetime

class Command(BaseCommand):
    # Show this when the user types help
    help = "Check tour paid status"

    # A command must define handle()
    def handle(self, *args, **options):
        tours = Tour.objects.all().filter(status = 1, paid = 0)
        for tour in tours:
            if tour.pay_date != None and tour.pay_date < datetime.datetime.now().date():
                tour.status = 3
                tour.save()
                # send notification to client
                message = Message(sender = tour.consultant, receiver = tour.client, content = "Ошибка! Ваш тур был отменен, по причине не оплаты, для подробностей свяжитесь с консультантом.")
                message.save()
                self.stdout.write('Changed status tour: "%ss"' % tour.id)
        