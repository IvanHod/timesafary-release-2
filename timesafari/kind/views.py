from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404
from .models import Kind
from .serializers import KindSerializer, KindUpdateSerializer
from rest_framework import permissions
from .permissions import KindPermission
from timesafari.exceptions import DeletionError
from django.db.models.deletion import ProtectedError

class KindCreateView(generics.CreateAPIView):
	serializer_class = KindUpdateSerializer
	permission_classes = (KindPermission, )

class KindListView(generics.ListAPIView):
	serializer_class = KindSerializer
	permission_classes = (permissions.AllowAny, )

	def get_queryset(self):
		queryset = Kind.objects.all()
		era_id = self.request.query_params.get('era', None)
		if era_id is not None:
			queryset = queryset.filter(era__id = era_id)
		return queryset

class KindRetrieveView(generics.RetrieveAPIView):
	serializer_class = KindSerializer
	queryset = Kind.objects.all()
	permission_classes = (permissions.AllowAny, )

class KindUpdateView(generics.UpdateAPIView):
	serializer_class = KindUpdateSerializer
	queryset = Kind.objects.all()
	permission_classes = (KindPermission, )

class KindDeleteView(generics.DestroyAPIView):
	serializer_class = KindSerializer
	queryset = Kind.objects.all()
	permission_classes = (KindPermission, )
	
	def perform_destroy(self, instance):
		try:
			instance.delete()
		except ProtectedError:
			raise DeletionError
	

"""
class KindDetail(APIView):
	def get_object(self, pk):
		try:
			return Kind.objects.get(pk=pk)
		except Kind.DoesNotExist:
			raise Http404

	def get(self, request, pk, format=None):
		kind = self.get_object(pk)
		serializer = KindSerializer(kind)
		return Response(serializer.data)

	def put(self, request, pk, format=None):
		kind = self.get_object(pk)
		serializer = KindSerializer(kind, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def delete(self, request, pk, format=None):
		kind = self.get_object(pk)
		kind.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)
"""