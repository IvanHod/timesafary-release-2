from rest_framework import serializers
from .models import Kind
from timesafari.era.serializers import EraDisplaySerializer

class KindSerializer(serializers.ModelSerializer):
	era = EraDisplaySerializer()
	class Meta:
		model = Kind
		fields = ('id', 'name', 'era', 'danger', 'description', 
			'picture', 'price', 'count', 'created', 'updated', )

class KindUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = Kind
		fields = ('id', 'name', 'era', 'danger', 'description', 
			'picture', 'price', 'count', 'created', 'updated', )
