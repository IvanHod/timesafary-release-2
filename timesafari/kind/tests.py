from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token
from timesafari.user.models import TimeSafariUser
from timesafari.era.models import Era
from .models import Kind
from django.core.exceptions import ObjectDoesNotExist

from django.core.files.images import ImageFile
import tempfile
from django.test.utils import override_settings

def get_test_image_file():
    file = tempfile.NamedTemporaryFile(suffix='.jpg')
    image = ImageFile(file, name=file.name)
    return image

MEDIA_ROOT = tempfile.mkdtemp()

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class CreateKindTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		Era.objects.create(name='era 1', price=1)
		Era.objects.create(name='era 2', price=2)

		self.url = reverse('kind-create')
		
	def test_create_kind_without_login(self):
		data = {'name': 'new kind', 'price': 300, 'era': 1, 'danger': 0, 'count': 10}
		response = self.guest.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

	def test_create_kind_without_permission(self):
		data = {'name': 'new kind', 'price': 300, 'era': 1, 'danger': 0, 'count': 10}
		response = self.client.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

	def test_create_kind_with_empty_name(self):
		data = {'name': '', 'price': 300, 'era': 1, 'danger': 0, 'count': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_create_kind_with_empty_price(self):
		data = {'name': 'new kind', 'era': 1, 'danger': 0, 'count': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Kind.objects.all()), 1)
		kind = Kind.objects.get(id=1)
		self.assertEqual(kind.name, 'new kind')
		self.assertEqual(kind.price, 0)

	def test_create_kind_with_empty_count(self):
		data = {'name': 'new kind', 'price': 20, 'era': 1, 'danger': 0}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Kind.objects.all()), 1)
		kind = Kind.objects.get(id=1)
		self.assertEqual(kind.name, 'new kind')
		self.assertEqual(kind.count, 0)

	def test_create_kind_with_negative_price(self):
		data = {'name': 'new kind', 'price': -10, 'era': 1, 'danger': 0, 'count': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Kind.objects.all()), 0)

	def test_create_kind_with_negative_count(self):
		data = {'name': 'new kind', 'price': 20, 'era': 1, 'danger': 0, 'count': -10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Kind.objects.all()), 0)

	def test_create_kind_with_empty_era(self):
		data = {'name': 'new kind', 'price': 20, 'danger': 0, 'count': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Kind.objects.all()), 0)

	def test_create_kind_with_none_exist_era(self):
		data = {'name': 'new kind', 'price': 20, 'danger': 0, 'count': 10, 'era': 200}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Kind.objects.all()), 0)

	def test_create_kind_with_empty_danger(self):
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Kind.objects.all()), 0)

	def test_create_kind_with_none_exist_danger(self):
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 10}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
		self.assertEqual(len(Kind.objects.all()), 0)

	def test_create_kind_with_picture(self):
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 2, 'picture': picture}
		response = self.admin.post(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Kind.objects.all()), 1)

	def test_create_kind_with_description(self):
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 2, 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.post(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Kind.objects.all()), 1)

	def test_create_era_with_all_fields(self):
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 2, 'picture': picture, 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.post(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_201_CREATED)
		self.assertEqual(len(Kind.objects.all()), 1)

@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetKindListTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('kind-list')

		Era.objects.create(name='era 1', price=1)
		Era.objects.create(name='era 2', price=2)
		for i in range(10):
			self.create_new_kind()

	def create_new_kind(self):
		Kind.objects.create(name='new kind', price=100, count=10, era=Era.objects.get(id=1), danger=0)

	def test_get_all_kinds_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)		

	def test_get_all_kinds_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)

	def test_get_all_kinds_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(response.data), 10)


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class GetKindDetailTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('kind-detail', args = [5])

		Era.objects.create(name='era 1', price=1)
		Era.objects.create(name='era 2', price=2)
		for i in range(10):
			self.create_new_kind('kind ' + str(i))

	def create_new_kind(self, name):
		Kind.objects.create(name=name, price=100, count=10, era=Era.objects.get(id=1), danger=0)

	def test_get_kind_without_login(self):
		response = self.guest.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(response.data['id'], 5)
		self.assertEqual(response.data['name'], 'kind 4')

	def test_get_kind_with_client(self):
		response = self.client.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(response.data['id'], 5)
		self.assertEqual(response.data['name'], 'kind 4')

	def test_get_kind_with_admin(self):
		response = self.admin.get(self.url)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(response.data['id'], 5)
		self.assertEqual(response.data['name'], 'kind 4')

	def test_get_none_exist_kind(self):
		url = reverse('kind-detail', args = [200])
		response = self.admin.get(url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class EditKindTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('kind-edit', args = [5])

		Era.objects.create(name='era 1', price=1)
		Era.objects.create(name='era 2', price=2)
		for i in range(10):
			self.create_new_kind('kind ' + str(i))		
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		self.data = {'name': 'new kind', 'price': 10, 'picture': picture, 'count': 10, 'era': 2, 'danger': 3}

	def create_new_kind(self, name):
		Kind.objects.create(name=name, price=100, count=10, era=Era.objects.get(id=1), danger=0,
			description='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.',
			picture='MonolophosaurusHiRes_usl6ti.jpg')

	def test_edit_kind_without_login(self):
		response = self.guest.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'kind 4')

	def test_edit_kind_with_client(self):
		response = self.client.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'kind 4')

	def test_edit_kind_with_admin(self):
		response = self.admin.put(self.url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'new kind')

	def test_edit_none_exist_kind(self):
		url = reverse('kind-edit', args = [200])
		response = self.admin.put(url, data=self.data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

	def test_edit_kind_with_empty_name(self):
		data = {'name': '', 'price': 300, 'era': 1, 'danger': 0, 'count': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_kind_with_empty_price(self):
		data = {'name': 'new kind', 'era': 1, 'danger': 0, 'count': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(Kind.objects.all()), 10)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'new kind')

	def test_edit_kind_with_empty_count(self):
		data = {'name': 'new kind', 'price': 20, 'era': 1, 'danger': 0}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(Kind.objects.all()), 10)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'new kind')
		self.assertEqual(kind.count, 10)

	def test_edit_kind_with_negative_price(self):
		data = {'name': 'new kind', 'price': -10, 'era': 1, 'danger': 0, 'count': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_kind_with_negative_count(self):
		data = {'name': 'new kind', 'price': 20, 'era': 1, 'danger': 0, 'count': -10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_kind_with_empty_era(self):
		data = {'name': 'new kind', 'price': 20, 'danger': 0, 'count': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_kind_with_none_exist_era(self):
		data = {'name': 'new kind', 'price': 20, 'danger': 0, 'count': 10, 'era': 200}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_kind_with_empty_danger(self):
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_edit_kind_with_none_exist_danger(self):
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 10}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

	def test_create_kind_with_picture(self):
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 2, 'picture': picture}
		response = self.admin.put(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(Kind.objects.all()), 10)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'new kind')

	def test_create_kind_with_description(self):
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 2, 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.put(self.url, data=data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(Kind.objects.all()), 10)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'new kind')

	def test_edit_era_with_all_fields(self):
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		data = {'name': 'new kind', 'price': 20, 'count': 10, 'era': 1, 'danger': 2, 'picture': picture, 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.'}
		response = self.admin.put(self.url, data=data, format='multipart')
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(len(Kind.objects.all()), 10)
		kind = Kind.objects.get(id=5)
		self.assertEqual(kind.name, 'new kind')


@override_settings(MEDIA_ROOT=MEDIA_ROOT)
class DeleteKindTest(APITestCase):
	fixtures = ['permissions.json', 'groups.json']

	def setUp(self):
		TimeSafariUser.objects.create_superuser('admin@admin.com', 'admin', 'admin', 'admin')
		TimeSafariUser.objects.create_user('client@client.com', 'client', 'client', 'client')
		url = reverse('user-login')
		data = {'email': 'client@client.com', 'password': 'client'}
		self.client.post(url, data=data)
		data = {'email': 'admin@admin.com', 'password': 'admin'}
		self.client.post(url, data=data)

		token = Token.objects.get(user__email='client@client.com')
		self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		token = Token.objects.get(user__email='admin@admin.com')
		self.admin = APIClient()
		self.admin.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

		self.guest = APIClient()

		self.url = reverse('kind-delete', args = [7])

		Era.objects.create(name='era 1', price=1)
		Era.objects.create(name='era 2', price=2)
		for i in range(10):
			self.create_new_kind('kind ' + str(i))		
		picture = open('D:\\MonolophosaurusHiRes_usl6ti.jpg', 'rb')
		self.data = {'name': 'new kind', 'price': 10, 'picture': picture, 'count': 10, 'era': 2, 'danger': 3}

	def create_new_kind(self, name):
		Kind.objects.create(name=name, price=100, count=10, era=Era.objects.get(id=1), danger=0,
			description='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut tortor non nisl molestie dictum eu ut massa. In dolor purus, aliquet a quam eget, rhoncus porttitor neque. Sed cursus dictum posuere. Vivamus sodales efficitur diam et dictum. Donec et leo mauris. Sed tortor mauris, condimentum nec nisl non, vehicula tempor felis. Nam volutpat nec nisi sed euismod.',
			picture='MonolophosaurusHiRes_usl6ti.jpg')


	def test_delete_kind_without_login(self):
		response = self.guest.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
		kind = Kind.objects.get(id=7)
		self.assertEqual(kind.name, 'kind 6')

	def test_delete_kind_with_client(self):
		response = self.client.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
		kind = Kind.objects.get(id=7)
		self.assertEqual(kind.name, 'kind 6')

	def test_delete_kind_with_admin(self):
		response = self.admin.delete(self.url)
		self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
		try:
			kind = Kind.objects.get(id=7)
		except ObjectDoesNotExist:
			pass
		else:
			assert False, "Object was not deleted!"

	def test_delete_none_exist_kind(self):
		url = reverse('kind-delete', args = [200])
		response = self.admin.delete(url)
		self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
