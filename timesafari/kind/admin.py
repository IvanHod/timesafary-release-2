from django.contrib import admin
from .models import Kind

class KindAdmin(admin.ModelAdmin):
	model = Kind
	list_display = ['name', 'era', ]


admin.site.register(Kind, KindAdmin)
