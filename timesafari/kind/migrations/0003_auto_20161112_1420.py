# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-12 11:20
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kind', '0002_auto_20161104_0202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kind',
            name='count',
            field=models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)]),
        ),
        migrations.AlterField(
            model_name='kind',
            name='price',
            field=models.PositiveIntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)]),
        ),
    ]
