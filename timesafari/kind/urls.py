from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
	# /kind/
	url(r'^$', views.KindListView.as_view(), name='kind-list'),
	# /kind/get/
	url(r'^get/$', views.KindListView.as_view(), name='kind-list'),
	# /kind/<pk>/
	url(r'^(?P<pk>[0-9]+)/$', views.KindRetrieveView.as_view(), name='kind-detail'),
	# /kind/get/<pk>/
	url(r'^get/(?P<pk>[0-9]+)/$', views.KindRetrieveView.as_view(), name='kind-detail'),
	# /kind/add/
	url(r'^add/$', views.KindCreateView.as_view(), name='kind-create'),
	# /kind/edit/<pk>/
	url(r'^edit/(?P<pk>[0-9]+)/$', views.KindUpdateView.as_view(), name='kind-edit'),
	# /kind/delete/<pk>/
	url(r'^delete/(?P<pk>[0-9]+)/$', views.KindDeleteView.as_view(), name='kind-delete'),
]

urlpatterns = format_suffix_patterns(urlpatterns)